## CocosCreator自动打包工具
##### 开发环境及语言：nodejs+TypeScript
##### 使用步骤：
- 命令行安装必备依赖模块npm install
- 首次使用需要编译打包工具 tsc --build
- 拷贝当前目录下的pack.config.yml配置文件到需要打包的工程的settings文件夹目录下
- 修改CocosCreator工程settings文件夹下pack.config.yml里的相关配置
- 执行打包命令 node ./build/app.js -p 项目路径 -c 打包的渠道平台

##### 目前支持打包渠道
1. 微信小游戏 可自动上传到后台
2. 字节跳动 可自动上传到后台
3. oppo/vivo/小米小游戏 打包出rpk文件
4. 快手小游戏  打包出zip压缩包，不支持上传后台，快手没提供对应ci工具
5. 百度小游戏 可自动上传到后台
6. h5 自动提交到服务器
7. 原生安卓等渠道 打包出apk

#### 命令详解
#### Options:
-p,--path           打包的项目路径(必填)

-c,--channel        打包对应的渠道(必填)

-d,--debug          是否是debug版本(可选，默认false,目前是对特定项目有用，可忽略)

-v                  打包的版本号(可选,格式x.x.x，如果传入则以传入版本号为准，否则以pack.config.yml与本地缓存的pack.config.json配置检测比较取版本号最大值为准) 

-h,--help           帮助说明(可选)