import { PackManager } from "./pack/PackManager";
let i = 2;
let projectDir = "";
let channel = "";
let isdebug = false;
let version: string | null = null!;
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = '1';
while (i < process.argv.length) {
    let arg = process.argv[i];
    switch (arg) {
        case '--path':
        case '-p':
            projectDir = process.argv[i + 1];
            i += 2;
            break;
        case '--channel':
        case '-c':
            channel = process.argv[i + 1];
            i += 2;
            break;
        case '--debug':
        case '-d':
            isdebug = process.argv[i + 1] == "true" ? true : false;
            i += 2;
            break;
        case '-v':
            version = process.argv[i + 1];
            i += 2;
            break;
        case '-h':
        case '--help':
            console.log("Options:\n-p,--path           打包的项目路径(必填)\n-c,--channel        打包对应的渠道(必填)\n-d,--debug          是否是debug版本(可选，默认false,目前是对特定项目有用，可忽略)\n-v                  打包的版本号(可选,格式x.x.x，如果传入则以传入版本号为准，否则以pack.config.yml与本地缓存的pack.config.json配置检测比较取版本号最大值为准) \n-h,--help           帮助说明(可选)");
            i++;
            break;
        default:
            i++;
            break;
    }
}
if (projectDir && channel) {
    PackManager.init(projectDir, channel, isdebug, version);
}