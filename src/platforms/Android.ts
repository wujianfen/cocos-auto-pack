import { execFile, execFileSync, execSync, spawn, spawnSync } from "child_process";
import { BasePlatform } from "./BasePlatform";
import path from 'path';
import PackUtil from "../utils/PackUtil";
import { ChannelInfo } from "./PlatformConfig";
import { readFileSync, writeFileSync } from "fs";
export class Android extends BasePlatform {
    private _androidDir: string = "";
    public async afterBuildFinish() {
        let channelInfo: ChannelInfo = this.configData.platforms[this.curPackChannel];
        //真正打包
        this._androidDir = path.join(this.outputPath, `jsb-${channelInfo.template}`, 'frameworks', 'runtime-src', 'proj.android-studio');
        console.log('start to build APK...', this._androidDir, Number(channelInfo.apiLevel));
        let code = PackUtil.appVersionToCode(this.version);
        const gpPath = path.join(this._androidDir, 'gradle.properties');
        //修改配置
        // let gpData = readFileSync(gpPath).toString();
        // gpData = gpData.replace(/PROP_COMPILE_SDK_VERSION=.*/, `PROP_COMPILE_SDK_VERSION=${Number(channelInfo.apiLevel) || 29}`);
        // gpData = gpData.replace(/PROP_TARGET_SDK_VERSION=.*/, `PROP_TARGET_SDK_VERSION=${Number(channelInfo.apiLevel) || 29}`);
        // gpData = gpData.replace(/RELEASE_STORE_FILE=.*/, `RELEASE_STORE_FILE=${channelInfo.keystorePath}`);
        // gpData = gpData.replace(/RELEASE_STORE_PASSWORD=.*/, `RELEASE_STORE_PASSWORD=${channelInfo.keystorePassword}`);
        // gpData = gpData.replace(/RELEASE_KEY_ALIAS=.*/, `RELEASE_KEY_ALIAS=${channelInfo.keystoreAlias}`);
        // gpData = gpData.replace(/RELEASE_KEY_PASSWORD=.*/, `RELEASE_KEY_PASSWORD=${channelInfo.keystoreAliasPassword}`);
        // writeFileSync(gpPath, gpData);

        //修改包名
        const abgPath = path.join(this._androidDir, 'app', 'build.gradle');
        let gradleData = readFileSync(abgPath).toString();
        let matcher1 = /applicationId[ ]+\"[a-zA-Z0-9_.]+\"/;
        gradleData = gradleData.replace(matcher1, `applicationId "${channelInfo.packageName}"`);
        writeFileSync(abgPath, gradleData);
        return new Promise<void>((resolve, reject) => {
            let oparray = ['assembleRelease', `-PVERSION_CODE=${code}`, `-PVERSION_NAME=${this.version}`];
            if (this.configData.apksOutput) {
                let outputapks = path.join(this.configData.apksOutput, this.curPackChannel + (this.isDebug ? "_debug" : ""));
                this.outputPath = outputapks;
                oparray.push(`-POUT_PUT_DIR=${outputapks}`);
            }
            let pro = execFile('gradlew.bat', oparray, { cwd: this._androidDir });
            pro.stdout?.on('data', (data) => {
                console.log(data);
            })
            pro.stdout?.on('error', (data) => {
                console.error("fasheng错误", data);
                reject(null);
            })
            pro.stderr?.on('data', (data) => {
                console.log(data);
                // throw Error();
            })
            pro.on('exit', (code, sign) => {
                if (code == 0) {
                    //上传
                    resolve();
                    super.afterBuildFinish();
                } else {
                    reject(sign);
                    throw Error();
                }
            });
            pro.on('error', (data) => {
                console.log("fa生错误", data);
            })
        });

    }

    public async build(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            let configData = this.configData;
            let channelInfo = this.channelInfo;
            let buildPath = this.outputPath;
            //构建信息
            let buildInfo = `"title=${configData.title};platform=${channelInfo.platform};buildPath=${buildPath};debug=false;md5Cache=${channelInfo.md5Cache};packageName=${channelInfo.packageName};useDebugKeystore=false;keystorePath=${channelInfo.keystorePath};keystorePassword=${channelInfo.keystorePassword};keystoreAlias=${channelInfo.keystoreAlias};keystoreAliasPassword=${channelInfo.keystoreAliasPassword};template=${channelInfo.template};orientation={'portrait':${this.configData.orientation == 'portrait'},'landscapeLeft':${this.configData.orientation != 'portrait'}};apiLevel=android-${Number(channelInfo.apiLevel) || 29};appABIs=['armeabi-v7a'];"`;
            try {
                console.log("building...");
                let sp = spawn(`${this.enginePath}`, ["--path", `${this.projectDir}`, "--build", `${buildInfo}`]);
                sp.stdout.setEncoding('utf8');
                sp.stdout.on('data', (data) => {
                    console.log(data);
                });
                sp.stdout.on('error', (data) => {
                    console.error(data);
                })
                sp.stderr.on('data', (data) => {
                    console.log(data)
                })
                sp.on('exit', (code, sign) => {
                    if (code == 0) {
                        console.log(`build finish ${this.curPackChannel}`);
                        resolve(true);
                    } else {
                        throw Error();
                    }
                });
                sp.on('error', (data) => {
                    console.log(data);
                })
                console.log("output path = ", buildPath)
            } catch (error) {
                console.log(`build failed ${this.curPackChannel}:${error}`);
                reject(false);
                throw Error();
            }
        });
    }
}