import { execFileSync, execSync, spawn, spawnSync } from "child_process";
import { BasePlatform } from "./BasePlatform";
import path from 'path';
import PackUtil from "../utils/PackUtil";
import { ChannelInfo } from "./PlatformConfig";
import { existsSync } from "fs";
export class KwaiGame extends BasePlatform {
    public async afterBuildFinish() {
        let channelInfo: ChannelInfo = this.configData.platforms[this.curPackChannel];
        let remoteDir = PackUtil.compareVersion(this.configData.engineVer, "2.4.0") >= 0 ? "remote" : "res";
        let remotepath = path.join(this.outputPath, channelInfo.platform, remoteDir);
        // let remotepath = path.join(this.outputPath, channelInfo.platform, "remote");
        if (existsSync(remotepath)) {
            console.log('upload cdn res start');
            let cdnrespath = channelInfo.remoteDir;
            PackUtil.pullCDNRes(cdnrespath);
            PackUtil.moveFiles(remotepath, cdnrespath);
            PackUtil.uploadCDNRes(cdnrespath);
            console.log('upload cdn res finish');
        }
        console.log('start pack kwai');
        try {
            let joinpath = `${this.configData.outputDir}/${this.channelInfo.buildPath}/${channelInfo.platform}`;
            spawnSync(this.configData.gitBashPath, ["-c", `sh ${channelInfo.toolsDir}/export_kwai.sh -z ${joinpath} -o ${this.configData.outputDir}/${this.channelInfo.buildPath} -v ${this.version}`]);
            console.log('pack kwai finish');
            this.saveBackUpConfig();
        } catch (error) {
            console.error(error);
        }
        await super.afterBuildFinish();
    }
}