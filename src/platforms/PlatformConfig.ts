import { Android } from "./Android";
import { BaiDu } from "./BaiDu";
import { BasePlatform } from "./BasePlatform";
import { ByteDance } from "./ByteDance";
import { KwaiGame } from "./KwaiGame";
import { OppoMicro } from "./OppoMicro";
import { VivoMicro } from "./VivoMicro";
import { WebMobile } from "./WebMobile";
import { WechatGame } from "./WechatGame";
import { XiaomiMicro } from "./XiaomiMicro";
export type Constructor<T = {}> = new (...args: any[]) => T;

export interface SupportPlatform {
    [key: string]: Constructor<BasePlatform>;
}
export interface ChannelToName {
    [key: string]: string,
}

export const supportPlatform: SupportPlatform = {
    "bytedance": ByteDance,
    "kwai": KwaiGame,
    "oppo": OppoMicro,
    "vivo": VivoMicro,
    "web_mobile": WebMobile,
    "wechatgame": WechatGame,
    "zijiaandroid": Android,
    "taptapandroid": Android,
    "mmyandroid": Android,
    "baidu": BaiDu,
    "xiaomi": XiaomiMicro,
    "googleandroid": Android,
}

export const channelToName: ChannelToName = {
    "bytedance": "字节跳动平台",
    "kwai": "快手小游戏",
    "oppo": "Oppo小游戏",
    "vivo": "Vivo小游戏",
    "web_mobile": "H5",
    "wechatgame": "微信小游戏",
    "zijiaandroid": "自家聚合安卓",
    "taptapandroid": "taptap安卓",
    "mmyandroid": "摸摸鱼安卓",
    "baidu": "百度小游戏",
    "xiaomi": "小米快游戏",
    "googleandroid": "谷歌安卓",
}

export const AutoUploadPlatform: ChannelToName = {
    "bytedance": "1",
    "wechatgame": "1",
    "baidu": "1",
}

export const EPlatform = {
    "web_mobile": 1,
    "zijiaandroid": 2,
    "juheios": 3,
    "wechatgame": 4,//微信小游戏
    "bytedance": 5,//字节跳动
    "qq": 6,//qq小游戏
    "baidu": 7,//百度小游戏
    "qttgame": 8,//趣头条
    "jkwgame": 9,//即可玩
    "oppoandroid": 10,   // OPPO
    "oppo": 11,     // OPPO 快游戏
    "vivo": 12,     //vivo小游戏
    "vivoandroid": 13,       // vivo
    "xiaomiandroid": 14,      // xiaomi
    "uc": 15,
    "googleandroid": 16,      //谷歌android
    "xiaomi": 17,//小米快游戏
    "kwai": 18,//快手
    "4399android": 19,//4399安卓
    "huaweiandroid": 20,//华为海外安卓
    "meizu": 21,
    "huawei": 22,//华为小游戏
    "taptapandroid": 23,//taptap
    "mmyandroid": 24,//mmy
};

export interface ChannelInfo {
    version: string,
    isNative: boolean,
    md5Cache: boolean,
    account?: string,
    password?: string,
    buildPath: string,
    platform: string,
    remoteDir: string,
    toolsDir?: string,
    privateKey?: string,//私钥 微信
    privatePath?: string,//快游戏rpk需要的
    certificatePath?: string,//快游戏rpk需要的
    icon?: string,//图标路径
    keystorePath?: string,
    packageName?: string,
    keystorePassword?: string,
    keystoreAlias?: string,
    keystoreAliasPassword?: string,
    template?: string,
    apiLevel?: number,
    serverPath?: string,
}
